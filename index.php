<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">

    <title>Mastodon Agregator</title>
    <link rel="icon" type="image/png" href="favicon.ico" />
    <link href="style.css" rel="stylesheet">

</head>

<body>
    <div class="mainContainer">
	<div class="normal" style="left: 0px">
	    <h1>mamot.fr</h1>
	    <iframe src="https://mamot.fr/web/timelines/public/local" class="iframe1"></iframe>
	</div>
	<div class="normal" style="left: 20vw">
	    <h1>mastodon.art</h1>
	    <iframe src="https://mastodon.art/web/timelines/public/local" ></iframe>
	</div>
	<div class="normal" style="left: 40vw">
	    <h1>miaou.drycat.fr</h1>
	    <iframe src="https://miaou.drycat.fr/web/timelines/public/local" ></iframe>
	</div>
	<div class="normal" style="left: 60vw">
	    <h1>mastodon.pirateparty.be</h1>
	    <iframe src="https://mastodon.pirateparty.be/web/timelines/public/local" ></iframe>
	</div>
	<div class="normal" style="left: 80vw">
	    <h1>emacs.li</h1>
	    <iframe src="https://emacs.li/web/timelines/public/local" ></iframe>
	</div>

	<div class="normal2" style="left: 0px">
	    <h1>hostux.social</h1>
	    <iframe src="https://hostux.social/web/timelines/public/local" ></iframe>
	</div>
	<div class="normal2" style="left: 20vw">
	    <h1>social.taker.fr</h1>
	    <iframe src="https://social.taker.fr/web/timelines/public/local" ></iframe>
	</div>
	<div class="normal2" style="left: 40vw">
	    <h1>mastodon.crypt.lol</h1>
	    <iframe src="https://mastodon.crypt.lol/web/timelines/public/local" ></iframe>
	</div>
	<div class="normal2" style="left: 60vw">
	    <h1>oc.todon.fr</h1>
	    <iframe src="https://oc.todon.fr/web/timelines/public/local" ></iframe>
	</div>
	<div class="normal2" style="left: 80vw">
	    <h1>i.write.codethat.sucks</h1>
	    <iframe src="https://i.write.codethat.sucks/web/timelines/public/local" ></iframe>
	</div>
	<!-- https://cybre.space/web/timelines/public/local -->
    </div>
</body>
</html>
